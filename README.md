# Product Owner Test 1

A idéia por trás deste teste é nos permitir avaliar parte das habilidades das pessoas candidatas a vagas da área de produto (Product Owner, Product Manager, etc), em variados níveis de experiência.

Este teste deve ser feito por você sozinho, no seu próprio tempo, em sua casa. Leve o tempo que precisar para concluí-lo, porém geralmente este teste não deve requisitar mais do que 2 horas para terminar.

Em caso de dúvidas, re-leia atentamente este documento. A compreensão do teste é parte do mesmo.

## Descrição

Imagine que você é o Product Owner responsável pelo [GMail](https://www.google.com/gmail/about/), o serviço de e-mails do [Google](https://www.google.com/). Como PO, você é o principal responsável por definir e priorizar os recursos e funcionalidades deste serviço, do ponto de vista dos seus usuários finais: as pessoas que usam o GMail para receber e enviar e-mails.

Você deve criar uma nova funcionalidade para o GMail, ou uma alteração em alguma funcionalidade existente.

Descreva o problema que está querendo resolver com esta mudança, e como sua idéia irá resolver este problema. Descreva o perfil dos usuários afetados, como eles são atualmente afetados pelo problema (se forem) e como a implementação da mudança os afetaria. Deixe claro como sua idéia agregaria valor ao produto, quais os custos envolvidos (inclusive custos de oportunidade), quais os recursos necessários para implementar a idéia, etc.

Fique à vontade para buscar dados e estimativas que suportem sua tese (ou mesmo inventá-los, desde que acompanhado de explicações sobre como os dados inventados teriam sido adquiridos no caso de um projeto real). Recomendamos também discorrer sobre como seria seu modo de trabalho para investigar o problema e chegar à solução proposta.

## Instruções de entrega

Para produzir o seu conteúdo, utilize qualquer aplicação desejada (editor de texto, sistemas de apresentações, planilhas, etc.). Entretanto, é necessário que tudo seja entregue em **um único arquivo PDF**. Você pode enviá-lo anexado em um e-mail para `apply@nuuvem.recruitee.com`.

Se tiver problemas para enviar o arquivo PDF como anexo por e-mail devido a restrições de tamanho, você pode hospedá-lo em serviços como Dropbox ou Google Drive e enviar o link para download por e-mail.

## Avaliação

Seu teste será avaliado seguindo os seguintes critérios:

- Aderência aos requisitos e instruções do teste;
- Qualidade da escrita, do ponto de vista gramatical e ortográfico;
- Qualidade do texto: clareza, coerência, apresentação e formatação, objetividade;
- Experiência demonstrada.

A qualidade da idéia em si (o quanto inovadora for a idéia de mudança no GMail) não importa. Estamos querendo ver como você trabalha, como chega a conclusões baseado em dados, como trabalha com outros para desenvolver e implementar soluções, como descreve para o resto do time o que deve ser implementado e o que está fora do escopo desejado, etc. Não estamos avaliando sua criatividade e nem sua familiaridade (ou não) com o GMail.

Feedbacks sobre o teste serão dados apenas oralmente, durante entrevistas posteriores ao envio do teste, se houver.
